"""Pytest customization

1. The tweak for enabling headless browser mode is adapted from
   [Support for headless Chrome #135](https://github.com/pytest-dev/pytest-selenium/issues/135)
2. Inserting tcid into JUnit XML files when the "tcid" mark is used
"""
import pytest


def pytest_addoption(parser):
    parser.addoption('--headless-browser', action='store_true',
                     help='enable browser headless mode')


def pytest_configure(config):
    config.addinivalue_line('markers', 'tcid(tcid): mark test-case id')


def pytest_runtest_setup(item):
    try:
        tcid = list(item.iter_markers(name='tcid'))[0].args[0]
        item.tcid = tcid
    except (TypeError, IndexError):
        pass


@pytest.fixture(scope='function', autouse=True)
def log_tcid(request, record_testsuite_property):
    try:
        module_name = request.cls.__name__ if request.cls is not None else request.module.__name__
        func_name = request.node.name
        record_testsuite_property(f'tcid:{module_name}-{func_name}', request.node.tcid)
    except AttributeError:
        pass


@pytest.fixture
def chrome_options(chrome_options, pytestconfig):
    if pytestconfig.getoption('--headless-browser'):
        chrome_options.add_argument('--headless')
    return chrome_options


@pytest.fixture
def firefox_options(firefox_options, pytestconfig):
    if pytestconfig.getoption('--headless-browser'):
        firefox_options.add_argument('-headless')
    return firefox_options
