import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException


class TestWeb:
    @classmethod
    def get_base_url(cls):
        return 'http://localhost:5000'

    @classmethod
    def set_symbol(cls, selenium, symbol: str):
        Select(selenium.find_element(By.CSS_SELECTOR, 'select[name=symbol]')).select_by_value(symbol)

    @classmethod
    def set_start_date(cls, selenium, year: str, month: str, day: str):
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=start_year]')
        input_elm.clear()
        input_elm.send_keys(year)
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=start_month]')
        input_elm.clear()
        input_elm.send_keys(month)
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=start_day]')
        input_elm.clear()
        input_elm.send_keys(day)

    @classmethod
    def set_end_date(cls, selenium, year: str, month: str, day: str):
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=end_year]')
        input_elm.clear()
        input_elm.send_keys(year)
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=end_month]')
        input_elm.clear()
        input_elm.send_keys(month)
        input_elm = selenium.find_element(By.CSS_SELECTOR, 'input[name=end_day]')
        input_elm.clear()
        input_elm.send_keys(day)

    @classmethod
    def click_submit(cls, selenium):
        selenium.find_element(By.CSS_SELECTOR, 'input[type=submit][value=Submit]').click()

    @pytest.mark.tcid(3522)
    def test_normal_1(self, selenium):
        selenium.get(self.get_base_url() + '/')
        assert selenium.title == 'USA Finance'

        self.set_symbol(selenium, 'AMZN')
        self.set_start_date(selenium, '2017', '1', '1')
        self.set_end_date(selenium, '2017', '1', '10')
        self.click_submit(selenium)

        # Verification
        assert selenium.title == 'USA Finance'
        assert selenium.find_element(By.CSS_SELECTOR, 'h2').text == 'AMZN'
        assert len(selenium.find_element(By.CSS_SELECTOR, 'body > table').find_elements(By.TAG_NAME, 'tr')) == 6

    @pytest.mark.tcid(3523)
    def test_normal_2(self, selenium):
        selenium.get(self.get_base_url() + '/')
        assert selenium.title == 'USA Finance'

        self.set_symbol(selenium, 'NFLX')
        self.set_start_date(selenium, '2016', '10', '5')
        self.set_end_date(selenium, '2016', '11', '1')
        self.click_submit(selenium)

        # Verification
        assert selenium.title == 'USA Finance'
        assert selenium.find_element(By.CSS_SELECTOR, 'h2').text == 'NFLX'
        assert len(selenium.find_element(By.CSS_SELECTOR, 'body > table').find_elements(By.TAG_NAME, 'tr')) == 20

    @pytest.mark.tcid(3524)
    def test_empty_month(self, selenium):
        selenium.get(self.get_base_url() + '/')
        assert selenium.title == 'USA Finance'

        self.set_symbol(selenium, 'NFLX')
        self.set_start_date(selenium, '2016', '', '5')
        self.set_end_date(selenium, '2016', '11', '1')
        self.click_submit(selenium)

        # Verification
        assert selenium.title == 'USA Finance'
        with pytest.raises(NoSuchElementException):
            selenium.find_element(By.CSS_SELECTOR, 'h2')

    @pytest.mark.tcid(3525)
    def test_until_before_since(self, selenium):
        selenium.get(self.get_base_url() + '/')
        assert selenium.title == 'USA Finance'

        self.set_symbol(selenium, 'NFLX')
        self.set_start_date(selenium, '2016', '10', '10')
        self.set_end_date(selenium, '2016', '10', '5')
        self.click_submit(selenium)

        # Verification
        assert selenium.title == 'USA Finance'
        assert selenium.find_element(By.CSS_SELECTOR, 'h2').text == 'NFLX'
        assert len(selenium.find_element(By.CSS_SELECTOR, 'body > table').find_elements(By.TAG_NAME, 'tr')) == 1
