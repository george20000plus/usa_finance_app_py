from datetime import datetime

import pytest

import routes


class TestRoutes:

    @pytest.mark.tcid(3519)
    def test_search_historical_data__normal(self):
        results = routes.search_historical_data('GOOG',
                                                datetime.strptime('2021-01-01', '%Y-%m-%d').date(),
                                                datetime.strptime('2021-02-01', '%Y-%m-%d').date())
        assert len(results) > 0

    @pytest.mark.tcid(3520)
    def test_search_historical_data__single_digit(self):
        results = routes.search_historical_data('GOOG',
                                                datetime.strptime('2021-1-1', '%Y-%m-%d').date(),
                                                datetime.strptime('2021-2-1', '%Y-%m-%d').date())
        assert len(results) > 0

    @pytest.mark.tcid(3521)
    def test_search_historical_data__empty_result(self):
        results = routes.search_historical_data('GOOG',
                                                datetime.strptime('2022-01-02', '%Y-%m-%d').date(),
                                                datetime.strptime('2022-01-02', '%Y-%m-%d').date())
        assert len(results) == 0
