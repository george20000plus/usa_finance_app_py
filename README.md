# USA Finance App

A basic USA finance look up app built upon Flask.

![Web page screenshot](rsc/p1.png)


## System Requirements

1. Python 3.8+

## Setup

1. (Optional) set up Python virtual environment and activate virtual environment.
2. Installed the required Python packages
    ```shell
    pip install -r requirements.txt
    ```

## Run the App

1. Run the Web App
    ```shell
    # internal-only server
    FLASK_APP=index flask run
    # externally-visible server
    FLASK_APP=index flask run --host=0.0.0.0
    ```
2. Visit the Web page `http://localhost:5000` (or `http://SERVER_IP:5000`)


## Test the App

Examples:

1. Unit test
   ```shell
   pytest tests/test_routes.py
   ```
2. Web test (Running App and browser with its Web driver are required)
   ```shell
   # Firefox
   pytest tests/integration/test_web.py --driver Firefox
   # Google Chrome
   pytest tests/integration/test_web.py --driver Chrome
   # Chromium
   pytest tests/integration/test_web.py --driver Chromium
   ```
3. Headless Web test (Running App and Web driver are required)
   ```shell
   # Firefox
   pytest tests/integration/test_web.py --driver Firefox --headless-browser
   # Google Chrome
   pytest tests/integration/test_web.py --driver Chrome --headless-browser
   # Chromium
   pytest tests/integration/test_web.py --driver Chromium --headless-browser
   ```

Notes:

1. To generate an HTML test report, add the Pytest arguments `--html $HTML_FILE_NAME --self-contained-html`.
2. To generate an XUnit-compatible XML test report, add the Pytest arguments `--junitxml $XML_FILE_NAME`

## Contact Us

Kuan-Li Peng (George.Peng@itri.org.tw)