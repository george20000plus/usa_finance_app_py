import yfinance as yf
from datetime import date
from flask import Blueprint, render_template, request


routed_pages = Blueprint('routed_pages', __name__, template_folder='templates')


def search_historical_data(symbol: str, start_date: date, end_date: date):
    ticker = yf.Ticker(symbol)
    return ticker.history(start=start_date, end=end_date)


@routed_pages.route('/')
def on_search_historical_data():
    query = request.args
    symbol = query.get('symbol')
    try:
        start_date = date(
                int(query.get('start_year')),
                int(query.get('start_month')),
                int(query.get('start_day'))).isoformat()
        end_date = date(
                int(query.get('end_year')),
                int(query.get('end_month')),
                int(query.get('end_day'))).isoformat()
    except (TypeError, ValueError) as err:
        start_date = None
        end_date = None

    print('symbol=' + str(symbol))
    print('start_date=' + str(start_date))
    print('end-date=' + str(end_date))

    if symbol is not None and start_date is not None and end_date is not None:
        share = search_historical_data(symbol, start_date, end_date)
        return render_template('index.html', symbol=symbol, results=share)
    return render_template('index.html')
